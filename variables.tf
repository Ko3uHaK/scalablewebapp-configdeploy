variable "az_subscription_id" {
}
variable "az_tenant_id" {

}
variable "az_client_id" {

}
variable "az_client_secret" {
}
variable "location" {
  description = "The location where the resource group, virtual network, and Kubernetes cluster will be created."
}

variable "resource_group_name" {
  description = "The name of the resource group."
}
variable "virtual_network_name" {
  description = "Name of the virtual network."
  type        = string
}

variable "address_space" {
  description = "Address space of the virtual network."
  type        = list(string)
}

variable "subnet_name" {
  description = "Name of the subnet."
  type        = string
}

variable "address_prefixes" {
  description = "Address prefixes of the subnet."
  type        = list(string)
}

variable "cluster_name" {
  description = "Name of the AKS cluster."
  type        = string
}

variable "dns_prefix" {
  description = "DNS prefix for the AKS cluster."
  type        = string
}

variable "ssh_public_key" {
  description = "SSH public key for authenticating to the VMs."
  type        = string
}

variable "admin_username" {
  description = "Admin username for the VMs."
  type        = string
}

variable "node_pool_name" {
  description = "Name of the node pool."
  type        = string
}

variable "node_vm_size" {
  description = "Size of the VMs in the node pool."
  type        = string
}

variable "node_pool_node_count" {
  description = "Number of nodes in the node pool."
  type        = number
}

variable "node_os_disk_size_gb" {
  description = "Size of the OS disk in GB for the VMs."
  type        = number
}