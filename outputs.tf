output "aks_cluster_id" {
  description = "ID of the AKS cluster."
  value       = module.aks-cluster.aks_cluster_id
}

output "aks_cluster_name" {
  description = "Name of the AKS cluster."
  value       = module.aks-cluster.aks_cluster_name
}