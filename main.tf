module "resource-group" {
  source              = "./modules/resource-group/"
  resource_group_name = var.resource_group_name
  location            = var.location
}

module "virtual-network" {
  source               = "./modules/virtual-network"
  location             = module.resource-group.location
  resource_group_name  = module.resource-group.resource_group_name
  virtual_network_name = var.virtual_network_name
  address_space        = var.address_space
  depends_on = [
    module.resource-group
  ]
}

module "subnet" {
  source               = "./modules/subnet"
  subnet_name          = var.subnet_name
  address_prefixes     = var.address_prefixes
  resource_group_name  = module.resource-group.resource_group_name
  virtual_network_name = module.virtual-network.virtual_network_name
  depends_on = [
    module.virtual-network
  ]
}
# module "public-ip" {
#   source              = "./modules/public-ip"
#   name                = "my-public-ip"
#   location            = module.resource-group.location
#   resource_group_name = module.resource-group.resource_group_name
#   allocation_method   = "Static"
#   depends_on = [
#     module.subnet
#   ]
# }

module "network-interface" {
  source                        = "./modules/network-interface"
  name                          = "my-nic"
  location                      = module.resource-group.location
  resource_group_name           = module.resource-group.resource_group_name
  subnet_id                     = module.subnet.subnet_id
  private_ip_address_allocation = "Dynamic"
  # public_ip_address_id          = module.public-ip.id
  # depends_on = [
  #   module.public-ip
  # ]
}
module "aks-cluster" {
  source               = "./modules/aks-cluster"
  cluster_name         = var.cluster_name
  location             = module.resource-group.location
  resource_group_name  = module.resource-group.resource_group_name
  dns_prefix           = var.dns_prefix
  ssh_public_key       = var.ssh_public_key
  admin_username       = var.admin_username
  node_pool_name       = var.node_pool_name
  node_vm_size         = var.node_vm_size
  node_pool_node_count = var.node_pool_node_count
  node_os_disk_size_gb = var.node_os_disk_size_gb
  vnet_subnet_id       = module.subnet.subnet_id
  az_client_id         = var.az_client_id
  az_client_secret     = var.az_client_secret
  depends_on = [
    module.network-interface
  ]
}