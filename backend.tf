terraform {
  backend "azurerm" {
    resource_group_name  = "rg-daniil-storage"
    storage_account_name = "devdaniil"
    container_name       = "application1"
    key                  = "terraform.tfstate"
  }
}