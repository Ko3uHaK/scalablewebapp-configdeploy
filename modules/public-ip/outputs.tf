output "id" {
  value       = azurerm_public_ip.example.id
  description = "ID of the created public IP address"
}

output "ip_address" {
  value       = azurerm_public_ip.example.ip_address
  description = "Public IP address"
}