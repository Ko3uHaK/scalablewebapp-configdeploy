variable "name" {
  type        = string
  description = "Name of the public IP address"
}

variable "resource_group_name" {
  type        = string
  description = "Name of the resource group where the public IP address will be created"
}

variable "location" {
  type        = string
  description = "Location of the public IP address"
}

variable "allocation_method" {
  type        = string
  description = "Allocation method of the public IP address"
}