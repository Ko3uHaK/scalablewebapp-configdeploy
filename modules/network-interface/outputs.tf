output "id" {
  value       = azurerm_network_interface.example.id
  description = "ID of the created network interface"
}

output "private_ip_address" {
  value       = azurerm_network_interface.example.ip_configuration[0].private_ip_address
  description = "Private IP address of the created network interface"
}

output "public_ip_address_id" {
  value       = azurerm_network_interface.example.ip_configuration[0].public_ip_address_id
  description = "ID of the associated public IP address"
}