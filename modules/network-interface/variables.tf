
variable "name" {
  type        = string
  description = "Name of the network interface"
}

variable "resource_group_name" {
  type        = string
  description = "Name of the resource group where the network interface will be created"
}

variable "location" {
  type        = string
  description = "Location of the network interface"
}

variable "subnet_id" {
  type        = string
  description = "ID of the subnet to associate with the network interface"
}

variable "private_ip_address_allocation" {
  type        = string
  description = "Private IP address allocation method"
}

# variable "public_ip_address_id" {
#   type        = string
#   description = "ID of the public IP address to associate with the network interface"
# }