output "resource_group_name" {
  value = azurerm_resource_group.rg.name
}
output "location" {
  value = azurerm_resource_group.rg.location
}
output "resource_group_id" {
  description = "ID of the resource group."
  value       = azurerm_resource_group.rg.id
}