resource "azurerm_kubernetes_cluster" "aks" {
  name                = var.cluster_name
  location            = var.location
  resource_group_name = var.resource_group_name
  dns_prefix          = var.dns_prefix

  linux_profile {
    admin_username = var.admin_username
          ssh_key {
        key_data = file(var.ssh_public_key)
    }
  }
  default_node_pool {
    temporary_name_for_rotation  = "poolrotation"
    name                = var.node_pool_name
    vm_size             = var.node_vm_size
    node_count          = var.node_pool_node_count
    os_disk_size_gb     = var.node_os_disk_size_gb
    vnet_subnet_id      = var.vnet_subnet_id
  }
  service_principal {
    client_id     = var.az_client_id
    client_secret = var.az_client_secret
  }
}