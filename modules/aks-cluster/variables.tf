variable "cluster_name" {
  description = "Name of the AKS cluster."
  type        = string
}

variable "dns_prefix" {
  description = "DNS prefix for the AKS cluster."
  type        = string
}

variable "node_pool_name" {
  description = "Name of the node pool."
  type        = string
}

variable "node_vm_size" {
  description = "Size of the VMs in the node pool."
  type        = string
}

variable "node_pool_node_count" {
  description = "Number of nodes in the node pool."
  type        = number
}

variable "node_os_disk_size_gb" {
  description = "Size of the OS disk for the nodes."
  type        = number
}

variable "vnet_subnet_id" {
  description = "ID of the subnet where the AKS cluster will be deployed."
  type        = string
}
variable "resource_group_name" {
  description = "Name of the resource group."
  type        = string
}

variable "location" {
  description = "Location for the resource group."
  type        = string
}
variable "admin_username" {
description = "The username for accessing the cluster nodes."
type = string
}

variable "ssh_public_key" {
description = "The public key used for SSH authentication to the cluster nodes."
type = string
}
variable "az_client_id" {
description = "The Azure client ID used for authentication and authorization."
type = string
}

variable "az_client_secret" {
description = "The Azure client secret used for authentication and authorization."
type = string
}